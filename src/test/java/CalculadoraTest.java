import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {
    @Test
    public void testSumaPositiva(){
        CalculadoraCasio calculadora=new CalculadoraCasio();
        Integer resultado=calculadora.sumar(5,5);
        Assertions.assertEquals(10,resultado);
    }
    @Test
    public void testSumaNegativa(){
        CalculadoraCasio calculadora=new CalculadoraCasio();
        Integer resultado=calculadora.sumar(-15,5);
        Assertions.assertEquals(-10,resultado);
    }

    @Test
    public void testRestaPositiva(){
        CalculadoraCasio calculadora=new CalculadoraCasio();
        Integer resultado=calculadora.restar(5,3);
        Assertions.assertEquals(2,resultado);
    }

    @Test
    public void testRestaNegativa(){
        CalculadoraCasio calculadora=new CalculadoraCasio();
        Integer resultado=calculadora.restar(-15,-4);
        Assertions.assertEquals(-11,resultado);
    }

    @Test
    public void testMultiplicacionPositiva(){
        CalculadoraCasio calculadora=new CalculadoraCasio();
        Integer resultado=calculadora.multiplicar(-6,-5);
        Assertions.assertEquals(30,resultado);
    }

    @Test
    public void testMultiplicacionNegativa(){
        CalculadoraCasio calculadora=new CalculadoraCasio();
        Integer resultado=calculadora.multiplicar(-5,5);
        Assertions.assertEquals(-25,resultado);
    }

    @Test
    public void testDivision(){
        CalculadoraCasio calculadora=new CalculadoraCasio();
        Integer resultado=calculadora.dividir(25,5);
        Assertions.assertEquals(5,resultado);
    }
}
