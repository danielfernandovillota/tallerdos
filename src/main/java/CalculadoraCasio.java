import java.util.ArrayList;
import java.util.List;

public class CalculadoraCasio implements Calculadora {
    String marca;
    String serial;
    String modelo;
    List<String> operaciones=new ArrayList<>();


    @Override
    public Integer sumar(Integer numeroUno, Integer numeroDos) {
        Integer resultado=numeroUno+numeroDos;
        this.operaciones.add("numero Uno: "+numeroUno+"+ numero Dos: "+numeroDos+" total: "+resultado);
        return resultado;
    }

    @Override
    public Integer restar(Integer numeroUno, Integer numeroDos) {
        Integer resultado=numeroUno-numeroDos;
        this.operaciones.add("numero Uno: "+numeroUno+"- numero Dos: "+numeroDos+" total: "+resultado);
        return numeroUno-numeroDos;
    }

    @Override
    public Integer multiplicar(Integer numeroUno, Integer numeroDos) {
        Integer resultado=numeroUno*numeroDos;
        this.operaciones.add("numero Uno: "+numeroUno+"* numero Dos: "+numeroDos+" total: "+resultado);
        return numeroUno*numeroDos;
    }

    @Override
    public Integer dividir(Integer numeroUno, Integer numeroDos) {
            Integer resultado=numeroUno/numeroDos;
            this.operaciones.add("numero Uno: "+numeroUno+"/ numero Dos: "+numeroDos+" total: "+resultado);
            return numeroUno/numeroDos;
    }
}
