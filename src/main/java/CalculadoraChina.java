public class CalculadoraChina implements Calculadora {
    String serial;

    @Override
    public Integer sumar(Integer numeroUno, Integer numeroDos) {
        return numeroUno+numeroDos;
    }

    @Override
    public Integer restar(Integer numeroUno, Integer numeroDos) {
        return numeroUno-numeroDos;
    }

    @Override
    public Integer multiplicar(Integer numeroUno, Integer numeroDos) {
        return numeroUno*numeroDos;
    }

    @Override
    public Integer dividir(Integer numeroUno, Integer numeroDos) {
        return numeroUno/numeroDos;
    }
}
