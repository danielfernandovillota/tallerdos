public interface Calculadora {
    public Integer sumar(Integer numeroUno, Integer numeroDos);
    public Integer restar(Integer numeroUno, Integer numeroDos);
    public Integer multiplicar(Integer numeroUno, Integer numeroDos);
    public Integer dividir(Integer numeroUno, Integer numeroDos);
}
