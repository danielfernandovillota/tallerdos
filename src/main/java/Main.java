public class Main {
    public static void main(String[] args){

        CalculadoraCasio calculadora=new CalculadoraCasio();
        calculadora.marca="Cassio";
        calculadora.serial="121222";
        calculadora.modelo="fx90";


        System.out.println("CALCULADORA CASIO");
        Integer suma=calculadora.sumar(1,2);
        System.out.println("La suma da como resultado: "+suma);
        Integer resta=calculadora.restar(5,2);
        System.out.println("La resta da como resultado: "+resta);
        Integer multiplicacion=calculadora.multiplicar(5,5);
        System.out.println("La multiplicacion da como resultado: "+multiplicacion);
        Integer division=calculadora.dividir(25,5);
        System.out.println("La division da como resultado: "+division);

        System.out.println("\nCALCULADORA CHINA");
        CalculadoraChina calculadoraChina=new CalculadoraChina();
        calculadoraChina.serial="012233";
        Integer sumaChina=calculadoraChina.sumar(2,2);
        System.out.println("La suma China es: "+sumaChina);
        Integer restaChina=calculadoraChina.restar(6,2);
        System.out.println("La resta China es: "+restaChina);
        Integer multiplicacionChina=calculadoraChina.multiplicar(6,6);
        System.out.println("La multiplicacion China es: "+multiplicacionChina);
        Integer divisionChina=calculadoraChina.dividir(15,3);
        System.out.println("La division China es: "+divisionChina);



    }
}
